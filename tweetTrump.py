# -*- coding: utf-8 -*-
"""
Created on Sun Jun  9 18:43:03 2019

@author: Felix Wangota
"""
#Opinions on Trump
import tweepy
from Tkinter import * #GUI Tool
from time import sleep
from datetime import datetime
from textblob import TextBlob #Language processing A.I
import matplotlib.pyplot as plt

#Authentication Keys
consumer_key = 'GiOVEQJZtlnattsMrtbl8FZcz'
consumer_secret = 'H81NrCevKWDvAgRzChUKm8rgJRDnnRqyZLMKrjW1peKOcUqlbq'
access_token = '726747169189945345-f51OldFujB2qGrg8Vi3frrsUkWby8sf'
access_token_secret = 'ECzoRAk6s3vbwtTZkhOEGt28ChYyGaoBXTxk9UAwnWhm5'

# Aunthentication Process
auth = tweepy.OAuthHandler(consumer_key,consumer_secret)
auth.set_access_token(access_token,access_token_secret)
api = tweepy.API(auth)

#GUI
root = Tk()


def getE1():
    E1 = "Trump"
    return E1

def getE2():
    E2 = 300
    return E2

def getData():
    getE1()
    keyword = getE1()

    getE2()
    numberOfTweets = getE2()
    numberOfTweets = int(numberOfTweets)

    #Where the tweets are stored to be plotted
    polarity_list = []
    numbers_list = []
    number = 1
    
    for  tweet in tweepy.Cursor(api.search, keyword, lang="en").items(numberOfTweets): #Not sure what this loop is doing though
        try:
            analysis = TextBlob(tweet.text) #converting each tweet into analysable text
            analysis = analysis.sentiment
            polarity = analysis.polarity
            polarity_list.append(polarity)
            numbers_list.append(number)
            number = number + 1
        except tweepy.TweepError as e:
            print(e.reason)
        except StopIteration:
            break        
    #Plotting
    axes = plt.gca()            
    axes.set_ylim([-1,2])
    
    plt.scatter(numbers_list,polarity_list)
    averagePolarity = (sum(polarity_list)/len(polarity_list))
    averagePolarity = "{0:.0f}%".format(averagePolarity * 100)
    time = datetime.now().strftime("At: %H:%M\nOn: %m-%d-%y")
    
    plt.text(0,1.25,"Average Sentiment: "+str(averagePolarity)+ "\n" +time, fontsize=12, bbox = dict(facecolor='none',edgecolor='black',boxstyle='square,pad=1'))
    plt.title("Sentiment of"+ keyword + "on Twitter")    
    plt.xlabel("Number of Tweets")
    plt.ylabel("Sentiment")
    plt.show()

submit = Button(root, text="Submit", command=getData())

label1.pack()
E1.pack()
label2.pack()
E2.pack()
submit.pack(side=BOTTOM)

root.mainloop()